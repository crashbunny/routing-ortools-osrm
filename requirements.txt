numpy
ortools
gdal==3.0.*
pynominatim
osrm
matplotlib

# Testing tools
vcrpy
coverage
coverage-badge

## Debug
pdbpp
