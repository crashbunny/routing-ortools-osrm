from routingoo.vrp_solver import VrpSolver

from .test_common import TestRouting


class TestSolvers(TestRouting):
    def test_show_logs(self):
        """Test is the show_log argument didn't break the solver methode
        """
        route = self.vrp_solver.solver_guided_local_search(
            self.distance_matrix, self.time_max, show_log=True
        )[0]
        self.total_distance = self.vrp_solver.compute_total_distance(
            route, self.distance_matrix
        )
        self.assertTrue(self.total_distance)

    def test_solver_guided_local_search_default_guided_local_search(self):
        """ test with:
            num_vehicles = 1
            distance_matrix=address 1,2,3
            time_max=2
            heuristic_type="FirstSolutionStrategy",
            heuristic="PATH_CHEAPEST_ARC",
            max_travel_distance=False,
            show_log=False
        """
        # route_0 is the path from the first location
        # to the first location include
        self.assertEqual(len(self.route_0), len(self.locations) + 1)
        self.assertEqual(self.route_0[0], self.route_0[-1])
        self.assertNotIn(self.route_0[1], self.route_0[2:])
        self.assertNotIn(self.route_0[-2], self.route_0[:-2])
        # create other not optimized route : route_bis
        # with permutation of the 2 first element
        route_bis = self.route_0[3:]
        route_bis.insert(0, self.route_0[1])
        route_bis.insert(0, self.route_0[2])
        route_bis.insert(0, self.route_0[0])
        total_distance_bis = self.vrp_solver.compute_total_distance(
            route_bis, self.distance_matrix
        )
        self.assertLess(self.total_distance, total_distance_bis)

    def test_solver_guided_local_search_localsearchmetaheuristic(self):
        """ test with:
            num_vehicles = 1
            distance_matrix=address 1,2,3
            time_max=2
            heuristic_type="LocalSearchMetaheuristic",
            heuristic="GUIDED_LOCAL_SEARCH",
            max_travel_distance=False,
            show_log=False
        """
        route_greedy_descent = self.vrp_solver.solver_guided_local_search(
            self.distance_matrix,
            self.time_max,
            heuristic_type="LocalSearchMetaheuristic",
            heuristic="GUIDED_LOCAL_SEARCH",
        )[0]
        self.total_distance = self.vrp_solver.compute_total_distance(
            route_greedy_descent, self.distance_matrix
        )
        self.assertEqual(len(route_greedy_descent), len(self.locations) + 1)
        self.assertEqual(route_greedy_descent[0], route_greedy_descent[-1])
        self.assertNotIn(route_greedy_descent[1], route_greedy_descent[2:])
        self.assertNotIn(route_greedy_descent[-2], route_greedy_descent[:-2])
        # create other not optimized route : route_bis
        # with permutation of the 2 first element
        route_bis = route_greedy_descent[3:]
        route_bis.insert(0, route_greedy_descent[1])
        route_bis.insert(0, route_greedy_descent[2])
        route_bis.insert(0, route_greedy_descent[0])
        total_distance_bis = self.vrp_solver.compute_total_distance(
            route_bis, self.distance_matrix
        )
        self.assertLess(self.total_distance, total_distance_bis)

    def test_solver_guided_local_search_localsearchmetaheuristic_big_matix(self):
        """ test with:
            num_vehicles = 1
            distance_matrix=big_distance_matrix
            time_max=2
            heuristic_type="LocalSearchMetaheuristic",
            heuristic="GUIDED_LOCAL_SEARCH",
            max_travel_distance=False,
            show_log=False
        """
        route_greedy_descent = self.vrp_solver.solver_guided_local_search(
            self.big_distance_matrix,
            self.time_max,
            heuristic_type="LocalSearchMetaheuristic",
            heuristic="GUIDED_LOCAL_SEARCH",
        )[0]
        self.total_distance = self.vrp_solver.compute_total_distance(
            route_greedy_descent, self.big_distance_matrix
        )
        self.assertEqual(len(route_greedy_descent), len(self.big_distance_matrix) + 1)
        self.assertEqual(route_greedy_descent[0], route_greedy_descent[-1])
        self.assertNotIn(route_greedy_descent[1], route_greedy_descent[2:])
        self.assertNotIn(route_greedy_descent[-2], route_greedy_descent[:-2])
        # create other not optimized route : route_bis
        # with permutation of the 2 first element
        route_bis = route_greedy_descent[3:]
        route_bis.insert(0, route_greedy_descent[1])
        route_bis.insert(0, route_greedy_descent[2])
        route_bis.insert(0, route_greedy_descent[0])
        total_distance_bis = self.vrp_solver.compute_total_distance(
            route_bis, self.big_distance_matrix
        )
        self.assertLess(self.total_distance, total_distance_bis)

    def test_solver_guided_local_search_wrong_heuristic(self):
        """ test with:
            num_vehicles = 1
            distance_matrix=big_distance_matrix
            time_max=2
            heuristic_type="FirstSolutionStrategy",
            heuristic="SPAN_AND_EGGS",
            max_travel_distance=False,
            show_log=False
        """
        try:
            self.vrp_solver.solver_guided_local_search(
                self.distance_matrix, self.time_max, heuristic="SPAN_AND_EGGS"
            )[0]
        except AttributeError as valueexept:
            wrong_heuristic_exeption = valueexept
        self.assertEqual(wrong_heuristic_exeption.args[0], "SPAN_AND_EGGS")

    def test_solver_path_cheapest_arc_with_many_vehicles_test_matrix_max_travel_distance_to_low(
        self
    ):
        """ test with:
            num_vehicles = range(1, 5)
            distance_matrix=test_distance_matrix
            time_max=4
            heuristic_type="FirstSolutionStrategy",
            heuristic="PATH_CHEAPEST_ARC",
            max_travel_distance=10,
            show_log=False
        """
        for num_vehicles in range(1, 5):
            self.vrp_solver = VrpSolver(num_vehicles)
            routes = self.vrp_solver.solver_guided_local_search(
                self.test_distance_matrix, time_max=4, max_travel_distance=10
            )
            self.assertFalse(routes)

    def test_solver_path_cheapest_arc_with_many_vehicles_test_matrix_max_travel_distance_height(
        self
    ):
        """ test with:
            num_vehicles = range(1, 5)
            distance_matrix=test_distance_matrix
            time_max=4
            heuristic_type="FirstSolutionStrategy",
            heuristic="PATH_CHEAPEST_ARC",
            max_travel_distance=1000,
            show_log=False
        """
        for num_vehicles in range(1, 5):
            self.vrp_solver = VrpSolver(num_vehicles)
            routes = self.vrp_solver.solver_guided_local_search(
                self.test_distance_matrix, time_max=4, max_travel_distance=1000
            )
            self.assertTrue(routes)
            self.assertEqual(len(routes), num_vehicles)

    def test_solver_path_cheapest_arc_with_many_vehicles_big_matrix_max_dist_not_set(
        self
    ):
        """ test with:
            num_vehicles = range(2, 5)
            distance_matrix=big_distance_matrix
            time_max=4
            heuristic_type="FirstSolutionStrategy",
            heuristic="PATH_CHEAPEST_ARC",
            max_travel_distance=1000,
            show_log=False
        """
        for num_vehicles in range(2, 5):
            self.vrp_solver = VrpSolver(num_vehicles)
            routes = self.vrp_solver.solver_guided_local_search(
                self.big_distance_matrix, time_max=4
            )
            for route in routes:
                total_distance = self.vrp_solver.compute_total_distance(
                    route, self.big_distance_matrix
                )
                self.assertTrue(route)
                self.assertTrue(total_distance)

    def test_solver_guided_local_search_with_many_vehicles_middle_matrixmax_dist_not_set(
        self
    ):
        """ test with:
            num_vehicles = 2
            distance_matrix=middle_distance_matrix
            time_max=8
            heuristic_type="FirstSolutionStrategy",
            heuristic="PATH_CHEAPEST_ARC",
            max_travel_distance=1000,
            show_log=False
        """
        num_vehicles = 2
        self.vrp_solver = VrpSolver(num_vehicles)
        routes = self.vrp_solver.solver_guided_local_search(
            self.middle_distance_matrix, time_max=8
        )
        self.assertEqual(len(routes), num_vehicles)
        for route in routes:
            self.assertTrue(route)
